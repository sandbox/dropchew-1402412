<?php
/**
 * @file
 * Provide views data and handlers for basic_page.module
 */

/**
 * Includes the ability to create views of any basic_page table.
 * @{
 */

/**
 * Implements hook_views_data()
 */
function basic_page_views_data() {
  // Basic table information.

  $data['custom_pages']['table']['group']  = t('Basic Pages');

    // Advertise this table as a possible base table
  $data['custom_pages']['table']['base'] = array(
    'field' => 'cpid',
    'title' => t('Basic Pages'),
    'weight' => 10,
  );
  
  // totalcount
  $data['custom_pages']['cpid'] = array(
    'title' => t('ID'),
    'help' => t('The ID of the basic page'),
	
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
     ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  // title
  $data['custom_pages']['title'] = array(
    'title' => t('Title'),
    'help' => t('Title of page.'),

    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
     ),
     'filter' => array(
      'handler' => 'views_handler_filter_string',
     ),
     'sort' => array(
      'handler' => 'views_handler_sort',
     ),
  );
  
  // timestamp
  $data['custom_pages']['updated'] = array(
    'title' => t('Updated date'),
    'help' => t('The most recent time the basic page has been updated.'),

    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  return $data;
}

/**
 * @}
 */
