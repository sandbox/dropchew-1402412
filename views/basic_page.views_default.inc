<?php

/**
 * @file
 * Default views for basic page.
 */
function basic_page_views_default_views() {
		
	$views = array();

	/*** Start Basic Pages View ***/
	
	$view = new view;
	$view->name = 'basic_pages';
	$view->description = '';
	$view->tag = 'default';
	$view->base_table = 'custom_pages';
	$view->human_name = 'Basic Pages';
	$view->core = 7;
	$view->api_version = '3.0-alpha1';
	$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

	/* Display: Master */
	$handler = $view->new_display('default', 'Master', 'default');
	$handler->display->display_options['use_ajax'] = TRUE;
	$handler->display->display_options['access']['type'] = 'none';
	$handler->display->display_options['cache']['type'] = 'none';
	$handler->display->display_options['query']['type'] = 'views_query';
	$handler->display->display_options['query']['options']['slave'] = TRUE;
	$handler->display->display_options['query']['options']['query_comment'] = TRUE;
	$handler->display->display_options['exposed_form']['type'] = 'basic';
	$handler->display->display_options['exposed_form']['options']['autosubmit'] = 0;
	$handler->display->display_options['exposed_form']['options']['autosubmit_hide'] = 1;
	$handler->display->display_options['pager']['type'] = 'full';
	$handler->display->display_options['pager']['options']['items_per_page'] = '12';
	$handler->display->display_options['pager']['options']['offset'] = '0';
	$handler->display->display_options['pager']['options']['id'] = '0';
	$handler->display->display_options['pager']['options']['expose']['items_per_page_options_all'] = 0;
	$handler->display->display_options['style_plugin'] = 'table';
	$handler->display->display_options['style_options']['columns'] = array(
	  'cpid' => 'cpid',
	  'title' => 'title',
	  'updated' => 'updated',
	  'nothing' => 'nothing',
	  'nothing_1' => 'nothing_1',
	);
	$handler->display->display_options['style_options']['default'] = 'updated';
	$handler->display->display_options['style_options']['info'] = array(
	  'cpid' => array(
		'sortable' => 0,
		'default_sort_order' => 'asc',
		'align' => '',
		'separator' => '',
	  ),
	  'title' => array(
		'sortable' => 1,
		'default_sort_order' => 'desc',
		'align' => 'views-align-left',
		'separator' => '',
	  ),
	  'updated' => array(
		'sortable' => 1,
		'default_sort_order' => 'desc',
		'align' => '',
		'separator' => '',
	  ),
	  'nothing' => array(
		'align' => '',
		'separator' => '',
	  ),
	  'nothing_1' => array(
		'align' => '',
		'separator' => '',
	  ),
	);
	$handler->display->display_options['style_options']['override'] = 1;
	$handler->display->display_options['style_options']['sticky'] = 0;
	$handler->display->display_options['style_options']['empty_table'] = 0;
	/* Field: Basic Pages: ID */
	$handler->display->display_options['fields']['cpid']['id'] = 'cpid';
	$handler->display->display_options['fields']['cpid']['table'] = 'custom_pages';
	$handler->display->display_options['fields']['cpid']['field'] = 'cpid';
	$handler->display->display_options['fields']['cpid']['label'] = '';
	$handler->display->display_options['fields']['cpid']['exclude'] = TRUE;
	$handler->display->display_options['fields']['cpid']['alter']['alter_text'] = 0;
	$handler->display->display_options['fields']['cpid']['alter']['make_link'] = 0;
	$handler->display->display_options['fields']['cpid']['alter']['absolute'] = 0;
	$handler->display->display_options['fields']['cpid']['alter']['external'] = 0;
	$handler->display->display_options['fields']['cpid']['alter']['replace_spaces'] = 0;
	$handler->display->display_options['fields']['cpid']['alter']['trim_whitespace'] = 0;
	$handler->display->display_options['fields']['cpid']['alter']['nl2br'] = 0;
	$handler->display->display_options['fields']['cpid']['alter']['word_boundary'] = 1;
	$handler->display->display_options['fields']['cpid']['alter']['ellipsis'] = 1;
	$handler->display->display_options['fields']['cpid']['alter']['strip_tags'] = 0;
	$handler->display->display_options['fields']['cpid']['alter']['trim'] = 0;
	$handler->display->display_options['fields']['cpid']['alter']['html'] = 0;
	$handler->display->display_options['fields']['cpid']['element_label_colon'] = FALSE;
	$handler->display->display_options['fields']['cpid']['element_default_classes'] = 1;
	$handler->display->display_options['fields']['cpid']['hide_empty'] = 0;
	$handler->display->display_options['fields']['cpid']['empty_zero'] = 0;
	$handler->display->display_options['fields']['cpid']['hide_alter_empty'] = 0;
	$handler->display->display_options['fields']['cpid']['format_plural'] = 0;
	/* Field: Basic Pages: Title */
	$handler->display->display_options['fields']['title']['id'] = 'title';
	$handler->display->display_options['fields']['title']['table'] = 'custom_pages';
	$handler->display->display_options['fields']['title']['field'] = 'title';
	$handler->display->display_options['fields']['title']['alter']['alter_text'] = 0;
	$handler->display->display_options['fields']['title']['alter']['make_link'] = 1;
	$handler->display->display_options['fields']['title']['alter']['path'] = 'basic-page/[cpid]';
	$handler->display->display_options['fields']['title']['alter']['absolute'] = 0;
	$handler->display->display_options['fields']['title']['alter']['external'] = 0;
	$handler->display->display_options['fields']['title']['alter']['replace_spaces'] = 0;
	$handler->display->display_options['fields']['title']['alter']['trim_whitespace'] = 0;
	$handler->display->display_options['fields']['title']['alter']['nl2br'] = 0;
	$handler->display->display_options['fields']['title']['alter']['word_boundary'] = 1;
	$handler->display->display_options['fields']['title']['alter']['ellipsis'] = 1;
	$handler->display->display_options['fields']['title']['alter']['strip_tags'] = 0;
	$handler->display->display_options['fields']['title']['alter']['trim'] = 0;
	$handler->display->display_options['fields']['title']['alter']['html'] = 0;
	$handler->display->display_options['fields']['title']['element_label_colon'] = 0;
	$handler->display->display_options['fields']['title']['element_default_classes'] = 1;
	$handler->display->display_options['fields']['title']['hide_empty'] = 0;
	$handler->display->display_options['fields']['title']['empty_zero'] = 0;
	$handler->display->display_options['fields']['title']['hide_alter_empty'] = 0;
	/* Field: Basic Pages: Updated date */
	$handler->display->display_options['fields']['updated']['id'] = 'updated';
	$handler->display->display_options['fields']['updated']['table'] = 'custom_pages';
	$handler->display->display_options['fields']['updated']['field'] = 'updated';
	$handler->display->display_options['fields']['updated']['label'] = 'Updated';
	$handler->display->display_options['fields']['updated']['alter']['alter_text'] = 0;
	$handler->display->display_options['fields']['updated']['alter']['make_link'] = 0;
	$handler->display->display_options['fields']['updated']['alter']['absolute'] = 0;
	$handler->display->display_options['fields']['updated']['alter']['external'] = 0;
	$handler->display->display_options['fields']['updated']['alter']['replace_spaces'] = 0;
	$handler->display->display_options['fields']['updated']['alter']['trim_whitespace'] = 0;
	$handler->display->display_options['fields']['updated']['alter']['nl2br'] = 0;
	$handler->display->display_options['fields']['updated']['alter']['word_boundary'] = 1;
	$handler->display->display_options['fields']['updated']['alter']['ellipsis'] = 1;
	$handler->display->display_options['fields']['updated']['alter']['strip_tags'] = 0;
	$handler->display->display_options['fields']['updated']['alter']['trim'] = 0;
	$handler->display->display_options['fields']['updated']['alter']['html'] = 0;
	$handler->display->display_options['fields']['updated']['element_label_colon'] = 0;
	$handler->display->display_options['fields']['updated']['element_default_classes'] = 1;
	$handler->display->display_options['fields']['updated']['hide_empty'] = 0;
	$handler->display->display_options['fields']['updated']['empty_zero'] = 0;
	$handler->display->display_options['fields']['updated']['hide_alter_empty'] = 0;
	$handler->display->display_options['fields']['updated']['date_format'] = 'time ago';
	/* Field: Global: Custom text */
	$handler->display->display_options['fields']['nothing']['id'] = 'nothing';
	$handler->display->display_options['fields']['nothing']['table'] = 'views';
	$handler->display->display_options['fields']['nothing']['field'] = 'nothing';
	$handler->display->display_options['fields']['nothing']['label'] = '';
	$handler->display->display_options['fields']['nothing']['alter']['text'] = 'edit';
	$handler->display->display_options['fields']['nothing']['alter']['make_link'] = 1;
	$handler->display->display_options['fields']['nothing']['alter']['path'] = 'basic-page/[cpid]/edit';
	$handler->display->display_options['fields']['nothing']['alter']['absolute'] = 0;
	$handler->display->display_options['fields']['nothing']['alter']['external'] = 0;
	$handler->display->display_options['fields']['nothing']['alter']['replace_spaces'] = 0;
	$handler->display->display_options['fields']['nothing']['alter']['trim_whitespace'] = 0;
	$handler->display->display_options['fields']['nothing']['alter']['nl2br'] = 0;
	$handler->display->display_options['fields']['nothing']['alter']['word_boundary'] = 1;
	$handler->display->display_options['fields']['nothing']['alter']['ellipsis'] = 1;
	$handler->display->display_options['fields']['nothing']['alter']['strip_tags'] = 0;
	$handler->display->display_options['fields']['nothing']['alter']['trim'] = 0;
	$handler->display->display_options['fields']['nothing']['alter']['html'] = 0;
	$handler->display->display_options['fields']['nothing']['element_label_colon'] = FALSE;
	$handler->display->display_options['fields']['nothing']['element_default_classes'] = 1;
	$handler->display->display_options['fields']['nothing']['hide_empty'] = 0;
	$handler->display->display_options['fields']['nothing']['empty_zero'] = 0;
	$handler->display->display_options['fields']['nothing']['hide_alter_empty'] = 0;
	/* Field: Global: Custom text */
	$handler->display->display_options['fields']['nothing_1']['id'] = 'nothing_1';
	$handler->display->display_options['fields']['nothing_1']['table'] = 'views';
	$handler->display->display_options['fields']['nothing_1']['field'] = 'nothing';
	$handler->display->display_options['fields']['nothing_1']['label'] = '';
	$handler->display->display_options['fields']['nothing_1']['alter']['text'] = 'delete';
	$handler->display->display_options['fields']['nothing_1']['alter']['make_link'] = 1;
	$handler->display->display_options['fields']['nothing_1']['alter']['path'] = 'basic-page/[cpid]/delete';
	$handler->display->display_options['fields']['nothing_1']['alter']['absolute'] = 0;
	$handler->display->display_options['fields']['nothing_1']['alter']['external'] = 0;
	$handler->display->display_options['fields']['nothing_1']['alter']['replace_spaces'] = 0;
	$handler->display->display_options['fields']['nothing_1']['alter']['trim_whitespace'] = 0;
	$handler->display->display_options['fields']['nothing_1']['alter']['nl2br'] = 0;
	$handler->display->display_options['fields']['nothing_1']['alter']['word_boundary'] = 1;
	$handler->display->display_options['fields']['nothing_1']['alter']['ellipsis'] = 1;
	$handler->display->display_options['fields']['nothing_1']['alter']['strip_tags'] = 0;
	$handler->display->display_options['fields']['nothing_1']['alter']['trim'] = 0;
	$handler->display->display_options['fields']['nothing_1']['alter']['html'] = 0;
	$handler->display->display_options['fields']['nothing_1']['element_label_colon'] = FALSE;
	$handler->display->display_options['fields']['nothing_1']['element_default_classes'] = 1;
	$handler->display->display_options['fields']['nothing_1']['hide_empty'] = 0;
	$handler->display->display_options['fields']['nothing_1']['empty_zero'] = 0;
	$handler->display->display_options['fields']['nothing_1']['hide_alter_empty'] = 0;

	/* Display: Block */
	$handler = $view->new_display('block', 'Block', 'block_1');
	$handler->display->display_options['display_comment'] = 'Block is displayed on the basic pages admin page.';
	$handler->display->display_options['display_description'] = 'For display at basic pages admin page.';
	
	$views[$view->name] = $view;
	
	/*** End Basic Pages View ***/
	
	return $views;
}
