<?php

/**
 * @file
 * view page callbacks for the asiaone_health_node_monitor module.
 */


function basic_page_render($cp){
	
	if (!$cp){
		return drupal_not_found();
	}
	
	if (user_access('administer basic page') || $cp->status) {
		return $cp->body;
	}

	return drupal_access_denied();
} 