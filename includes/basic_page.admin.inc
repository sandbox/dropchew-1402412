<?php

/**
 * @file
 * admin page callbacks for the asiaone_health_node_monitor module.
 */

function basic_page_administer(){
							
	$view_name = 'basic_pages'; //name of view
	$display_id = 'block_1';
	
	$html =  '<ul class="action-links">'
				. l(t('Add a basic page'), 'admin/structure/basic-pages/add') .
			'</ul>' . views_embed_view($view_name,$display_id);;

	return $html;
	
}

function basic_page_form($cp, $action){
	
	if (!is_object($cp) || $action != 'edit' && $action != 'delete') { return drupal_not_found(); }
	
	$form_callback = ($action == 'edit') ? 'basic_page_edit' : 'basic_page_delete';
	
	return drupal_get_form($form_callback, $cp);
}

function basic_page_create_form(){
	return drupal_get_form('basic_page_edit'); 
}

function basic_page_delete($form, &$form_state, $cp = null){

	$form['cp'] = array('#type' => 'value', '#value' => $cp );
	return confirm_form($form, t('Are you sure you want to delete the page %name?', array('%name' => $cp->title)), 'admin/structure/basic-pages', '', t('Delete'), t('Cancel'));
	
}

function basic_page_delete_submit($form, &$form_state){
	
	$title = $form['cp']['#value']->title;
	
	$deleted = db_delete('custom_pages')
	  ->condition('cpid', $form['cp']['#value']->cpid)
	  ->execute();
	
	$msgType = 'status';
	
	if ($deleted){
		path_delete(array('source' => 'basic-page/'.$form['cp']['#value']->cpid));
		$form_state['redirect'] = "admin/structure/basic-pages";
		$msg = "Basic page '$title' deleted successfully";
	}else{
		$msg = "An error occured while deleting the page. Please try again.";
		$msgType = 'error';
	}
	
	drupal_set_message($msg, $msgType);
}

function basic_page_edit($form, &$form_state, $cp = null){
	
	$title = $body = '';
	$status = 0;
	
	if ($cp) {
		$title = $cp->title;
		$body = $cp->body;
		$status = $cp->status;
	} 
	
	$form['ispublished'] = array(
	  '#type' => 'checkbox',
	  '#default_value' => $status,
	  '#title' => t('Publish this page'),
	);
	
	$form['title'] = array(
	  '#type' => 'textfield',
	  '#title' => t('Title'),
	  '#description' => t('The title of the page'),
	  '#default_value' => $title,
	  '#size' => 60,
	  '#maxlength' => 128,
	  '#required' => TRUE,
	);
		
	$form['body'] = array(
		'#type' => 'text_format',
		'#title' => t('Body'),
		'#description' => t('The body of the page'),
		'#default_value' => $body,
		'#format' => NULL,
	);
	
	$cpid = $cp ? $cp->cpid : '' ;
	$source = "basic-page/".$cpid;

	$form['alias'] = array(
		'#type' => 'textfield',
		'#title' => t('Path'),
		'#maxlength' => 255,
		'#size' => 45,
		'#default_value' => drupal_lookup_path('alias', $source),
		'#description' => t('Specify an alternative path by which this data can be accessed. For example, type "about" when writing an about page. Use a relative path and don\'t add a trailing slash or the URL alias won\'t work.'),
		'#field_prefix' => url(NULL, array('absolute' => TRUE)) . (variable_get('clean_url', 0) ? '' : '?q='),
	);
	
	$form['source'] = array(
		'#type' => 'hidden',
		'#value' => $source,
	);
	
	$form['cp'] = array('#type' => 'value', '#value' => $cp );
	$form['submit'] = array('#type' => 'submit', '#value' => t('Save'));
  
	return $form;
}

function basic_page_edit_validate($form, &$form_state){
	
	$source = &$form_state['values']['source'];
	$source = drupal_get_normal_path($source);
	$alias = $form_state['values']['alias'];
	
	$has_alias = db_query("SELECT COUNT(alias) FROM {url_alias} WHERE alias = :alias and source != :source", array(
      ':alias' => $alias,
	  ':source' => $source,
    ))
    ->fetchField();

  if ($has_alias) {
    form_set_error('alias', t('The alias %alias is already in use.', array('%alias' => $alias)));
  }

}

function basic_page_edit_submit($form, &$form_state){
	
	$cp = $form['cp']['#value'];
	$table = 'custom_pages';
	$title = $form_state['values']['title'];
	$body = $form_state['values']['body']['value'];
	$status = $form_state['values']['ispublished'];
	$msgType = 'status';
	
	if (!$cp){
	
		$cpid = db_insert($table)
		  ->fields(array(
			'title' => $title,
			'body' => $body,
			'status' => $status,
			'updated' => time(),
		  ))
		->execute();
		
		if ($cpid){
		
			$source = "basic-page/$cpid";
			$form_state['values']['source'] = $source;
			form_state_values_clean($form_state);
			
			if ($form_state['values']['alias'] != ''){
				path_save($form_state['values']);
			}
			
			$msg = "Basic page '$title' created successfully";
			$form_state['redirect'] = $source;
			
		}else{
			$msg = "An error occured while creating the page. Please try again.";
			$msgType = 'error';
		}
	} else {
		
		$updated = db_update($table)
		  ->fields(array(
			'title' => $title,
			'body' => $body,
			'status' => $status,
			'updated' => time(),
		  ))
		  ->condition('cpid', $cp->cpid)
		  ->execute();
		
		if ($updated){
			
			path_delete(array('source' => $form_state['values']['source']));
			
			if ($form_state['values']['alias'] != '') {
				path_save($form_state['values']);
			}
			
			$form_state['redirect'] = "basic-page/" . $cp->cpid;
			$msg = "Basic page '$title' updated successfully";

		}else{
			$msg = "An error occured while updating the page. Please try again.";
			$msgType = 'error';
		}
	
	}
	
	drupal_set_message($msg, $msgType);

}
